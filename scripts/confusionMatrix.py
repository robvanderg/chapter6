"""
Draw confusionmatrix based on tagged text
"""
import sys
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from pylab import *

plt.style.use('scripts/rob.mplstyle')
fig, ax = plt.subplots(figsize=(8,5), dpi=300)
tags = set(['^','~',',','!','@','$','&','#','A','D','E','G','L','M','N','O','P','R','S','T','U','V','X','Y','Z'])
tags = sorted(tags)
print(tags)

def getData(filename):
    data = []
    for i in range(len(tags)):
        data.append([0] * len(tags))

    for line in open(filename):
        if len(line) > 3:
            predIdx = tags.index(line.split()[2])
            goldIdx = tags.index(line.split()[1])
            if goldIdx != predIdx:
                data[predIdx][goldIdx] += 1
    return data
    
data3 = []
for i in range(len(tags)):
    data3.append([0] * len(tags))

if len(sys.argv) > 2:
    data1 = getData(sys.argv[1])
    data2 = getData(sys.argv[2])

    for i in range(len(tags)):
        print(data1[i])

    for i in range(len(tags)):
        for j in range(len(tags)):
            data3[i][j] = data1[i][j] - data2[i][j]

            if data3[i][j] > 20 or data3[i][j] < -20:
                print(data3[i][j], tags[i], tags[j])
else:
    data3 = getData(sys.argv[1])
    for line in data3:
        print(line)
#img=ax.matshow(data3, cmap=get_cmap('RdYlGn'), vmin=0, vmax=20)
#img=ax.matshow(data3, cmap=get_cmap('winter'), vmin=0, vmax=20)
#img=ax.matshow(data3, cmap=get_cmap('bone'), vmin=0, vmax=20)
img=ax.matshow(data3, cmap=get_cmap('YlOrRd'), vmin=0, vmax=21)
#img=ax.matshow(data3, cmap=get_cmap('jet'), vmin=0, vmax=20)

ax.grid(False)

ticks = []
for i in range (len(tags)):
    ticks.append(i)

ax.xaxis.set_major_locator(mpl.ticker.LinearLocator(len(tags)+1))
ax.xaxis.set_minor_locator(mpl.ticker.FixedLocator(ticks))
ax.xaxis.set_major_formatter(mpl.ticker.NullFormatter())
ax.xaxis.set_minor_formatter(mpl.ticker.FixedFormatter(tags))
for tick in ax.xaxis.get_minor_ticks():
    tick.tick1line.set_markersize(0)
    tick.tick2line.set_markersize(0)

ax.yaxis.set_major_formatter(mpl.ticker.NullFormatter())
ax.yaxis.set_minor_formatter(mpl.ticker.FixedFormatter(tags))
ax.yaxis.set_major_locator(mpl.ticker.LinearLocator(len(tags)+1))
ax.yaxis.set_minor_locator(mpl.ticker.FixedLocator(ticks))
for tick in ax.yaxis.get_minor_ticks():
    tick.tick1line.set_markersize(0)
    tick.tick2line.set_markersize(0)

plt.tight_layout()
ax.set_ylabel('Predicted label')
ax.set_xlabel('Gold label')
ax.xaxis.set_label_position('top') 

#cax = ax.imshow(data3, interpolation='nearest', vmin=0.5, vmax=0.99)
plt.colorbar(img, ticks=range(0,20,3))

fig.savefig("confusion.pdf", bbox_inches='tight')
#plt.show()

