import os

for i in range(100, 1700, 100):
    os.system("python3 scripts/cutData.py data/bilty/train_O.raw.pos " + str(i))
    train = 'data/bilty/train_O.raw.pos.' + str(i)
    testPath = 'data/bilty/dev_O.raw.pos'
    for seed in range(0,10):
        seed = str(seed)
        output = 'preds/4.dev_O.' + str(i) + '.' + seed
        cmd = 'python3 bilstm-aux/src/bilty.py --dynet-seed ' + seed + ' --dynet-mem 1500 --train ' + train + ' --pred_layer 1 --iters 10 --c_in_dim 256 --in_dim 100 --h_layers 1 --sigma 0.2 --trainer adam --initializer constant --test ' + testPath + ' &> ' + output
        print(cmd)

