import os

os.system('mkdir -p preds')

for seed in range(0,10):
    seed = str(seed)
    for mode in ['word', 'wang']:
        for dim in ['100', '400']:
            for window in ['1', '5']:
                embedPath = 'embeds/' + mode + '.' + dim + '.' + window + '.txt'
                testPath = 'data/bilty/dev_O.raw.pos'
                output = '.'.join(['preds/3.dev_O', 'raw', mode, dim, window, seed])
                cmd = 'python3 bilstm-aux/src/bilty.py --dynet-seed ' + seed + ' --dynet-mem 1500 --train data/bilty/train_O.raw.pos --pred_layer 1 --iters 10 --c_in_dim 256 --in_dim ' + dim + ' --h_layers 1 --sigma 0.2 --trainer adam --initializer constant --embeds ' + embedPath + ' --test ' + testPath + ' &> ' + output
                print(cmd)
                
    
