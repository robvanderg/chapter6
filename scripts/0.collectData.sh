mkdir data
mkdir data/raw
cd data/raw
wget http://www.hlt.utdallas.edu/~chenli/normalization_pos/test_set_1.txt
wget http://www.hlt.utdallas.edu/~chenli/normalization_pos/test_set_2.txt
wget https://raw.githubusercontent.com/brendano/ark-tweet-nlp/master/data/twpos-data-v0.3/full_data/oct27.supertsv
wget https://raw.githubusercontent.com/brendano/ark-tweet-nlp/master/data/twpos-data-v0.3/full_data/daily547.supertsv

grep "^ .*	G$" -v test_set_1.txt > tmp
mv tmp test_set_1.txt

echo "" > newline
cat daily547.supertsv newline oct27.supertsv | grep -v "G	 " > owoputi
rm newline

