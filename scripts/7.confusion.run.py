
for setting in ['norm', 'vec', 'comb']:
    seed = '1'#TODO how to better do this?
    output = 'preds/7.' + setting 
    dev = 'data/bilty/dev_O.raw.pos'
    if setting == 'norm' or setting == 'comb':
        dev = 'data/bilty/dev_O.normAll.pos'
    vec = ''
    if setting == 'vec' or setting == 'comb':
        vec = ' --embeds embeds/wang.100.1.txt '

    cmd = 'python3 bilstm-aux/src/bilty.py --dynet-seed ' + seed + ' --dynet-mem 1500 --train data/bilty/train_O.raw.pos --pred_layer 1 --iters 10 --c_in_dim 256 --in_dim 100 --h_layers 1 --sigma 0.2 --trainer adam --initializer constant' + vec + ' --test ' + dev + ' --output ' + output
    print(cmd)

print('java -cp ark-tweet-nlp-0.3.2/ark-tweet-nlp-0.3.2.jar cmu.arktweetnlp.RunTagger --input-format conll --model ark-tweet-nlp-0.3.2/model data/bilty/dev_O.raw.pos | cut -f 2 > preds/7.ark2', end= ' && ')
print('paste data/bilty/dev_O.raw.pos preds/7.ark2 | sed "s;^ $;;g" > preds/7.ark && rm preds/7.ark2')
