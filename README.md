# Normalization for POS tagging

This repository is for experiments testing the effect of normalization (https://bitbucket.org/robvanderg/monoise/src) on the POS tagging (https://github.com/bplank/bilstm-aux) of tweets.

### Re-run 
To re-run all experiments:
`
./scripts/runAll.sh
`

To generate all graphs/tables:
`
./scripts/genAll.sh
`
To rerun only a specific experiment, see the scripts folder.

These results are used in the following paper:
*To Normalize, or Not to
Normalize: The Impact of Normalization on Part-of-Speech Tagging*,
WNUT 2017, EMNLP 2017 workshop.

This is a revised repository which is used for the results in my thesis. It is much more easily reproducable compared to the original repository. The main differences are:
* Normalizing training data is now useful, because MoNoise is better compared to two years ago
* The exact same data as released by Chen Li is used, see (https://github.com/bplank/wnut-2017-pos-norm/)

### Reference

If you make use of the contents of this repository, we appreciate citing the following paper:

    @InProceedings{vandergoot:ea:2017:WNUT,
      author    = {van der Goot, Rob and Plank, Barbara and Nissim, Malvina},
      title     = {{To Normalize, or Not to Normalize: The Impact of Normalization on Part-of-Speech Tagging}},
      booktitle = {Proceedings of WNUT 2017},
      month     = {September},
      year      = {2017},
      address   = {Copenhagen, Denmark},
      publisher = {Association for Computational Linguistics},
    }


### Setup Details

`
DyNet 2.0

Python 3.6.4
`
