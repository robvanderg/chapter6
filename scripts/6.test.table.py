from scipy import stats


def getPerf(pred, gold):
    sents = []
    total = [0,0]
    sent = [0,0]
    with open(pred) as f1, open(gold) as f2:
        for predLine, goldLine in zip(f1, f2):
            if len(predLine) < 2:
                sents.append(sent[0] /  sent[1])
                sent = [0,0]
                continue        
            if predLine.split()[-1] == goldLine.split()[-1]:
                sent[0] += 1
                total[0] += 1
            sent[1] += 1
            total[1] += 1 
    return total[0]/total[1], sents

def avgList(list1):
    new = []
    for i in range(len(list1[0])):
        total = 0
        divide = 0
        for j in range(len(list1)):
            total += list1[j][i]
            divide += 1
        new.append(total/divide)
    return new

def isSign(list1, list2, val):
    avgList1 = avgList(list1)
    avgList2 = avgList(list2)
    return (stats.ttest_rel(avgList(list1), avgList(list2))[1] < val)

print('\\begin{table}')
print('    \\centering')
print('    \\begin{tabular}{l | l l l}')
print('        \\toprule')
print('                       & \\texttt{Dev\\_O}    & \\texttt{Test\\_O}  & \\texttt{Test\\_L} \\\\')
print('        \\midrule')
prev = {'dev_O':[], 'test_O':[], 'test_L':[]}
for mode in ['raw', 'norm', 'vec', 'combUnion']:
    modeTransl = {'raw':'Bilty', 'norm':'+Norm', 'vec':'+Embeds', 'comb':'+Comb', 'combUnion':'+Comb'}
    print ('        ' + modeTransl[mode].ljust(9) ,end='   ')
    for testSet in ['dev_O', 'test_O', 'test_L']:
        cur = []
        total = 0.0
        for seed in range(0,10):
            predPath = '.'.join(['preds/6', testSet , mode, str(seed), 'task0'])
            acc, accList = getPerf(predPath, 'data/bilty/' + testSet + '.raw.pos')
            total += acc
            cur.append(accList)
        sign = '' 
        if prev[testSet] != [] and isSign(cur, prev[testSet], 0.01):
            sign = '\\textsuperscript{*}'
        prev[testSet] = cur
    
        print('   & ' + str(round(total * 10,2)).ljust(5,'0'), end=sign +' ')
    print('\\\\')
print('        \\midrule')
print('        Ark        ', end='')
for testSet in ['dev_O', 'test_O', 'test_L']:
    acc, accList = getPerf('preds/6.ark.' + testSet, 'data/bilty/' + testSet + '.raw.pos') 
    arkList = [accList] * 10
    sign = ''
    if isSign(arkList, prev[testSet], 0.01):
        sign = '\\textsuperscript{*}'
    print('    & ' +  str(round(acc * 100, 2)).ljust(5,'0'), end=sign + ' ')
print(' \\\\')
print('        \\bottomrule')
print('    \\end{tabular}')
print('    \caption{Results on the test data compared to ARK-tagger~\citep{owoputi-EtAl:2013:NAACL-HLT}.  * Significant using a paired t-test at $p < 0.01$}')
print('    \label{tab:test3}')
print('\\end{table}')

