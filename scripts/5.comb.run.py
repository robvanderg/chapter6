import os

os.system('mkdir -p preds')

for seed in range(0,10):
    seed = str(seed)
    for setting in ['raw', 'norm', 'vec', 'comb', 'combUnion']:
        dev = 'data/bilty/dev_O.raw.pos'
        train = 'data/bilty/train_O.raw.pos' 
        vec = ''
        if setting == 'norm' or setting == 'comb' or setting == 'combUnion':
            dev = 'data/bilty/dev_O.normAll.pos'
        if setting == 'vec' or setting == 'comb' or setting == 'combUnion':
            vec = ' --embeds embeds/wang.100.1.txt '
        if setting == 'combUnion': 
            train = 'data/bilty/train_O.union.pos' 
        output = '.'.join(['preds/5.dev_O', setting, seed])
        cmd = 'python3 bilstm-aux/src/bilty.py --dynet-seed ' + seed + ' --dynet-mem 1500 --train ' + train + ' --pred_layer 1 --iters 10 --c_in_dim 256 --in_dim 100 --h_layers 1 --sigma 0.2 --trainer adam --initializer constant' + vec + ' --test ' + dev + ' --output ' + output
        print(cmd)
                
    
