import sys

data = ['']

for line in open(sys.argv[1]):
    tok = line.strip().split()
    if len(tok) == 0:
        data.append('')
    else:
        data[-1] += line

outFile = open(sys.argv[1] + '.' + sys.argv[2], 'w')
for i in range(0, int(sys.argv[2])):
    if i < len(data):
        outFile.write(data[i])
        if i != len(data) -1:
            outFile.write('\n')
outFile.close()
