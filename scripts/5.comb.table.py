
def getPerf(pred, gold, train):
    accKnowns = [0,0]
    accUnk = [0,0]
    knowns = set()
    for line in open(train):
        tok = line.split()
        if len(tok) > 0:
            knowns.add(tok[0])
    with open(pred) as f1, open(gold) as f2:
        for predLine, goldLine in zip(f1, f2):
            predTok = predLine.split()
            goldTok = goldLine.split()
            if len(goldTok) == 0:
                continue
            if goldTok[0] in knowns:
                if predTok[2] == goldTok[1]:
                    accKnowns[0] += 1
                else:
                    accKnowns[1] += 1
            else:
                if predTok[2] == goldTok[1]:
                    accUnk[0] += 1
                else:
                    accUnk[1] += 1
    accAll = [accKnowns[0] + accUnk[0], accKnowns[1] +  accUnk[1]]
    return accKnowns[0] / (accKnowns[0] + accKnowns[1]), accUnk[0] / (accUnk[0] + accUnk[1]), accAll[0] / (accAll[0] + accAll[1])

def getPerc(dev, train):
    knowns = set()
    knownCount = 0
    unkCount = 0
    for line in open(train):
        tok = line.split()
        if len(tok) > 0:
            knowns.add(tok[0])
    for line in open(dev):
        if len(line) < 2:
            continue
        word = line.split()[0]
        if word in knowns:
            knownCount += 1
        else:
            unkCount += 1
    total = unkCount + knownCount
    return knownCount / total, unkCount / total, total / total
        
    

print('\\begin{table}')
print('    \\centering')
print('    \\begin{tabular}{l | c | c | c c c}')
print('        \\toprule')
print('                    & \\% of data    & Bilty    & +Norm    & +Embeds  & +Comb \\\\')
print('        \\midrule')
for known in [0,1, 2]:
    transl = {0:'Known', 1:'Unknown', 2:'All'}
    if known == 2:
        print('        \\midrule')
    print('        ' + transl[known].ljust(9) ,end='   ')
    perc = getPerc('data/bilty/dev_O.raw.pos', 'data/bilty/train_O.raw.pos')[known] * 100
    print('& ' + str(round(perc, 2)), end = '  ')
    for mode in ['raw', 'norm', 'vec', 'comb']:
        total = 0.0
        for seed in range(0,10):
            predPath = '.'.join(['preds/5.dev_O', mode, str(seed), 'task0'])
            total += getPerf(predPath, 'data/bilty/dev_O.raw.pos', 'data/bilty/train_O.raw.pos')[known]
        print('   & ' + str(round(total * 10,2)).ljust(5,'0'), end=' ')
    print('\\\\')
print('        \\bottomrule')
print('    \\end{tabular}')
print('    \caption{Effect of different models on canonical/non-canonical words on development data (accuracy).}')
print('    \label{tab:perfNormed}')
print('\\end{table}')


