# If more than 5 arguments are given, it will run using slurm
function run {
    if [ "$6" ];
    then
        python3 ../scripts/pg.prep.py $1 $2 $3 $4 $5
        python3 ../scripts/pg.run.py $2.[0-9]*
    else
        chmod +x $1
        bash $1
    fi
}

# the following commands are left out, since their data is already in the repo
#./scripts/0.collectData.sh
#python3 scripts/0.splitData.py
#./scripts/0.cleanData.sh
#./scripts/2.pos.prep.sh
#./scripts/6.test.prep.sh

./scripts/0.collectEmbeds.sh

./scripts/1.norm.prep.sh 
./scripts/1.norm.train.sh > 1.train.sh
run 1.train.sh 1.train 2 40 8 $1
./scripts/1.norm.run.sh > 1.run.sh
run 1.run.sh 1.run 1 20 8 $1

python3 scripts/2.pos.run.py > 2.run.sh
run 2.run.sh 2.run 2 4 1 $1

python3 scripts/3.embeds.train.py | grep 400 > 3.400.sh
run 3.400.sh 3.400 2 85 1 $1

python3 scripts/3.embeds.train.py | grep -v 400 > 3.100.sh
run 3.100.sh 1.400 2 32 1 $1

python3 scripts/4.learningC.run.py > 4.learningC.sh
run 4.learningC.sh 4.learningC 2 4 1 $1

python3 scripts/5.comb.run.py | grep -v embed > 5.comb.sh
run 5.comb.sh 5.comb 2 4 1 $1

python3 scripts/5.comb.run.py | grep embed > 5.embed.sh
run 5.embed.sh 5.embed 2 32 1 $1

python3 scripts/6.test.run.py | grep -v embed > 6.test.sh
run 6.test.sh 6.test 2 4 1 $1

python3 scripts/6.test.run.py | grep embed > 6.embed.sh
run 6.embed.sh 6.embed 2 32 1 $1

python3 ./scripts/7.confusion.run.py > 7.confusion.sh
run 7.confusion.sh 7.confusion 2 32 1 $1

