import os

for testSet in ['dev_O', 'test_O', 'test_L']:
    cmd = 'java -cp ark-tweet-nlp-0.3.2/ark-tweet-nlp-0.3.2.jar cmu.arktweetnlp.RunTagger --input-format conll --model ark-tweet-nlp-0.3.2/model data/bilty/' + testSet + '.raw.pos | cut -f 1,2 > preds/6.ark.' + testSet
    print(cmd)

    for setting in ['raw', 'norm', 'vec', 'comb', 'combUnion']:
        for seed in range(0,10):
            seed = str(seed)
            output = 'preds/6.' + testSet + '.' + setting + '.' + seed
            dev = 'data/bilty/' + testSet + '.raw.pos'
            train = 'data/bilty/train_O.raw.pos'
            vec = ''
            if setting == 'norm' or setting == 'comb' or setting == 'combUnion':
                dev = 'data/bilty/' + testSet + '.normAll.pos'
                train = 'data/bilty/train_O.union.pos'
            if setting == 'vec' or setting == 'comb' or setting == 'combUnion':
                vec = ' --embeds embeds/wang.100.1.txt ' 
            if setting == 'combUnion':
                train = 'data/bilty/train_O.union.pos'

            cmd = 'python3 bilstm-aux/src/bilty.py --dynet-seed ' + seed + ' --dynet-mem 1500 --train ' + train + ' --pred_layer 1 --iters 10 --c_in_dim 256 --in_dim 100 --h_layers 1 --sigma 0.2 --trainer adam --initializer constant' + vec + ' --test ' + dev + ' --output ' + output
            print(cmd)




