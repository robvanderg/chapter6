# run different settings on dev
cd ../monoise/src && ./tmp/bin/binary -m RU -i ../../chapter6/data/dev_O.norm -d ../data/en -f 11110111111 -r ../working/chenliCaps.1 -C -W > ../../chapter6/data/dev_O.normAll
cd ../monoise/src && ./tmp/bin/binary -m RU -i ../../chapter6/data/dev_O.norm -d ../data/en -f 11110111111 -r ../working/chenliCaps.1 -C -W -u ../../chapter6/data/knowns > ../../chapter6/data/dev_O.normUnk
cd ../monoise/src && ./tmp/bin/binary -m RU -i ../../chapter6/data/dev_O.norm -d ../data/en -f 11110111111 -r ../working/chenliCaps.gold.1 -C -W -g > ../../chapter6/data/dev_O.normGoldED

# run normAll on train
cd ../monoise/src &&  ./tmp/bin/binary -m RU -i ../../chapter6/data/train_O.norm -d ../data/en -f 11110111111 -r ../working/chenliCaps.1 -C -W > ../../chapter6/data/train_O.normAll

# run normAll on test sets
cd ../monoise/src && ./tmp/bin/binary -m RU -i ../../chapter6/data/test_O.norm -d ../data/en -f 11110111111 -r ../working/chenliCaps.1 -C -W > ../../chapter6/data/test_O.normAll
cd ../monoise/src && ./tmp/bin/binary -m RU -i ../../chapter6/data/test_L.norm -d ../data/en -f 11110111111 -r ../working/chenliCaps.1 -C -W > ../../chapter6/data/test_L.normAll


