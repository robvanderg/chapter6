from scipy import stats

# run bilty for different normalization strategies

def getPerf(pred, gold):
    sents = []
    total = [0,0]
    sent = [0,0]
    with open(pred) as f1, open(gold) as f2:
        for predLine, goldLine in zip(f1, f2):
            if len(predLine) < 2:
                sents.append(sent[0] /  sent[1])
                sent = [0,0]
                continue
            if predLine.split()[-1] == goldLine.split()[-1]:
                sent[0] += 1
                total[0] += 1
            sent[1] += 1
            total[1] += 1
    return total[0]/total[1], sents

print('\\begin{table}')
print('    \\centering')
print('    \\begin{tabular}{l | l | l l}')
print('        \\toprule')
print('        $\\downarrow$ Test $\\rightarrow$ Train & \\texttt{Raw} & \\texttt{All} & \\texttt{Union} \\\\')
print('        \\midrule')

def avgList(list1):
    new = []
    for i in range(len(list1[0])):
        total = 0
        divide = 0
        for j in range(len(list1)):
            total += list1[j][i]
            divide += 1
        new.append(total/divide)
    return new

def isSign(list1, list2, val):
    avgList1 = avgList(list1)
    avgList2 = avgList(list2)
    return (stats.ttest_rel(avgList(list1), avgList(list2))[1]/2< val)

prevRaw = []
for test in ['raw', 'normUnk', 'normAll', 'normGoldED', 'normGold']:
    transl = {'raw':'Raw', 'normUnk':'Unk', 'normAll':'All', 'normGoldED':'GoldED', 'normGold':'Gold'}
    if (test == 'normUnk' or test == 'normGoldED'):
        print('        \\midrule')
    print('        \\texttt{' + (transl[test] + '}').ljust(12), end= '  &') 
    for train in ['raw', 'normAll', 'union']:
        cur = []
        total = 0.0
        for seed in range(0,10):
            predPath = 'preds/2.dev_O.' + train + '.' + test + '.' + str(seed) + '.task0'
            acc, resList = getPerf(predPath, 'data/bilty/dev_O.raw.pos')
            total += acc
            cur.append(resList)
        # get sign.
        sign = ''
        if prevRaw != []:
            if isSign(prevRaw, cur, 0.01):
                 sign = '\\textsuperscript{*}'
        print('    ' + str(round(total * 10,2)).ljust(5,'0'), end = sign + '  ')
        if train == 'raw':
            prevRaw = cur
        if train != 'union':
            print('  &', end='')
        else:
            print('  \\\\')


print('        \\bottomrule')
print('    \\end{tabular}')
print("""    \\caption{Results of normalization on the development data (macro average\n
over 5 runs).  \\texttt{Union} stands for the training set formed by the\n
concatenation of both normalized and original raw data. * Significant using a\n
paired t-test at $p < 0.01$ compared to the previous \\texttt{Raw Train}\n
setting""")
print('    \\label{tab:norm}')
print('\\end{table}')

