# create train concatenation
cat data/train_O data/train_O.normAll > data/train.union

mkdir preds
mkdir bilstm-aux/models

for file in data/*norm
do
    cut -f 3 $file > "$file"Gold
done
rm data/train_O.normGold

cut -f 2 data/train_O > data/train_O.pos

mkdir -p data/bilty

for file in dev_O.normAll dev_O.normUnk dev_O.normGold dev_O.normGoldED dev_O.raw
do
    paste data/$file data/dev_O.pos | sed "s;^	$;;g" > data/bilty/"$file".pos
done

paste data/train_O.normAll data/train_O.pos | sed "s;^	$;;g" > data/bilty/train_O.normAll.pos
paste data/train_O.raw data/train_O.pos | sed "s;^	$;;g" > data/bilty/train_O.raw.pos
cat data/bilty/train_O.normAll.pos data/bilty/train_O.raw.pos | sed "s;^	$;;g" > data/bilty/train_O.union.pos


paste data/test_O.raw data/test_O.pos | sed "s;^	$;;g" > data/bilty/test_O.raw.pos
paste data/test_L.raw data/test_L.pos | sed "s;^	$;;g" > data/bilty/test_L.raw.pos
paste data/test_L.normAll data/test_L.pos | sed "s;^	$;;g" > data/bilty/test_L.normAll.pos
paste data/test_O.normAll data/test_O.pos | sed "s;^	$;;g" > data/bilty/test_O.normAll.pos


#git clone https://github.com/bplank/bilstm-aux.git
#cd bilstm-aux
#git reset --hard 7b86e5cc5a4903be408583cd6ae5f1b96f3b301d
pip3 install --user dynet
pip3 install --user sklearn

