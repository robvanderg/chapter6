# run bilty for different normalization strategies

for seed in range(0,10):
    seed = str(seed)
    for train in ['raw', 'normAll', 'union']:
        modelPath = 'bilstm-aux/models/model.' + train + '.' + seed
        trainData = 'data/bilty/train_O.' + train + '.pos'
        cmd = 'python3 bilstm-aux/src/bilty.py --dynet-seed ' + seed + ' --dynet-mem 1500 --train ' + trainData + ' --save ' + modelPath + ' --pred_layer 1 --iters 10 --c_in_dim 256 --in_dim 100 --h_layers 1 --sigma 0.2 --trainer adam --initializer constant'
        print (cmd, end = ' ')
        for test in ['raw', 'normUnk', 'normAll', 'normGoldED', 'normGold']:
            testData = 'data/bilty/dev_O.' + test + '.pos'
            pred = 'preds/2.dev_O.' + train + '.' + test + '.' + seed
            cmd = ' && python3 bilstm-aux/src/bilty.py --dynet-seed ' + seed + ' --dynet-mem 1500 --model ' + modelPath + ' --test ' + testData + ' --pred_layer 1 --output ' + pred
            print (cmd, end= ' ')
        print()

