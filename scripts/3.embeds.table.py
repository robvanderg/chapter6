def getPerf(path):
    for line in open(path):
        if line.startswith('Task0 test accuracy on 1 items:'):
            return float(line.split()[-1])
    return 0.0

print('\\begin{table}')
print('    \\centering')
print('    \\begin{tabular}{l c c c c}')
print('        \\toprule')
print('        Dimensions             & \\multicolumn{2}{c}{100} & \\multicolumn{2}{c}{400} \\\\')
print('        Window size            & 1         & 5         & 1         & 5   \\\\')
print('        \\midrule')
for mode in ['word', 'wang']:
    transl = {'word':'Skipgrams', 'wang':'Structured Skipgrams'}
    print('        ' + transl[mode].ljust(20), end='')
    for dim in ['100', '400']:
        for window in ['1', '5']:
            total = 0.0
            for seed in range(0,10):
                predPath = '.'.join(['preds/3.dev_O.raw', mode, dim, window, str(seed)])
                total += getPerf(predPath)
            print('   & ' + str(round(total * 10,2)).ljust(5,'0'), end='  ')
    print('\\\\')
print('        \\bottomrule')
print('    \\end{tabular}')
print('    \caption{Accuracy on raw \\texttt{Dev}: various pre-trained skip-gram embeddings for initialization of the tagger.}')
print('    \label{tab:embeds}')
print('\\end{table}')
    
