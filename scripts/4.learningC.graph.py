import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

plt.style.use('scripts/rob.mplstyle')
fig, ax = plt.subplots(figsize=(8,5), dpi=300)

def getPerf(path):
    for line in open(path):
        if line.startswith('Task0 test accuracy on 1 items:'):
            return float(line.split()[-1])
    return 0.0


data = []
for i in range(100, 1700, 100):
    total = 0.0
    for seed in range(0,10):
        predPath = 'preds/4.dev_O.' + str(i) + '.' + str(seed)
        total += getPerf(predPath)
    data.append(total * 10)

print(data)
ax.plot(range(100, 1700, 100), data)
ax.set_ylim((60,85))
ax.set_xlim((0,1600))
ax.set_xlabel('Number of Tweets')
ax.set_ylabel('Accuracy')
fig.savefig('trainCurve.pdf', bbox_inches='tight')
plt.show()


