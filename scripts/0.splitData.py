"""
Extract the Tweets in owoputi's dataset that do not occur in dataset1, so that
we can use them to train a pos-tagger Also fixes some inconsistencies, and
generates test/dev sets.  Assumes data/raw/test_set_1.txt and data/raw/owoputi
are available in working directory.

Generates 
data/raw/test_set_1.txt.fixed: fixed version of test_set_1.txt
data/test_O: test split from owoputi data
data/test_L: test split from owoputi data
data/dev_O: dev split from owoputi data
data/train_O: all data not in Chen Li's data, used to train POS tagger
"""
import random
import os

random.seed(1234)

def levenshtein(s1, s2):
    if len(s1) < len(s2):
        return levenshtein(s2, s1)

    # len(s1) >= len(s2)
    if len(s2) == 0:
        return len(s1)

    previous_row = range(len(s2) + 1)
    for i, c1 in enumerate(s1):
        current_row = [i + 1]
        for j, c2 in enumerate(s2):
            insertions = previous_row[j + 1] + 1 
            deletions = current_row[j] + 1 
            substitutions = previous_row[j] + (c1 != c2)
            current_row.append(min(insertions, deletions, substitutions))
        previous_row = current_row

    return previous_row[-1]

def find(collection, sent):
    closestDist = 999
    closestSent = ''
    for cand in collection:
        dist = levenshtein(sent, cand)
        if dist < closestDist:
            closestDist = dist
            closestSent = cand
    return closestSent

chenli = {}

curSent = ''
curString = ''
for line in open('data/raw/test_set_1.txt', encoding='utf-8'):
    if len(line.split()) < 1:
        chenli[curSent] = curString
        curSent = ''
        curString = ''
    else:
        if len(line.split()) == 3:
            curSent += "ERR "
        else:
            curSent += line.split()[0].strip() + ' '
            curString += line
if len(curSent) > 1:
    chenli[curSent] = curString
    curSent = ''
    curString = ''

owoputi = {}
for line in open('data/raw/owoputi', encoding='utf-8'):
    if len(line.split()) < 1:
        if curSent in owoputi:
            print("error, sentence occures twice", curSent)
        owoputi[curSent] = curString
        curSent = ''
        curString = ''
    elif line[0] == 'T' and (line[1] == 'W' or line[1] == 'O'):
        pass
    else:
        if len(line.split()) == 1:
            curSent += "ERR "
        else:
            curSent += line.split()[1].strip() + ' '
        if len(line.split()) ==2:
            curString += line.split()[1] + '\t' + line.split()[0] + '\n'
if curSent in owoputi:
    print("error, sentence occures twice", curSent)
owoputi[curSent] = curString

def fix(orig, cor, ann):
    annTok = ann.split('\n')
    origTok = orig.split(' ')
    corTok = cor.split(' ')
    for i in range(len(corTok)):
        if i >= len(origTok) or origTok[i] != corTok[i]:
            annTok = annTok[:i] + ['??\tIV\t??\t,'] + annTok[i:] 
            break
    return [orig, cor, '\n'.join(annTok)]

repl = []
for sent in chenli:
    if sent not in owoputi:
        match = find(owoputi, sent)
        repl.append([sent, match, chenli[sent]])

for fix in repl:
    tmp = chenli[fix[0]]
    chenli[fix[1]] = fix[2]
    del chenli[fix[0]]

for sent in chenli:
    if sent not in owoputi:
        print("ERROR", sent)
        

fixedTest = open('data/raw/test_set_1.txt.fixed', 'w')
for sent in sorted(chenli):
    fixedTest.write(chenli[sent] + '\n')
fixedTest.close()

filtered = open('data/train_O', 'w')
shuffled = list(sorted(owoputi))
random.shuffle(shuffled)
for sent in shuffled:
    if sent not in chenli:
        filtered.write(owoputi[sent] + '\n')
filtered.close()


myList = []

sent = ''
for line in open('data/raw/test_set_1.txt.fixed'):
    if (len(line) < 3):
        myList.append(sent)
        sent = ''
    else:
        sent += line
myList.append(sent)

random.shuffle(myList)
testFile = open('data/test_O', 'w')
for item in myList[:549]:
    testFile.write(item + '\n')
testFile.close()
devFile = open('data/dev_O', 'w')
for item in myList[549:]:
    devFile.write(item + '\n')
devFile.close()

os.system('cp data/raw/test_set_2.txt data/test_L')

for path in ['data/dev_O', 'data/test_O', 'data/train_O', 'data/test_L']:
    cmd = 'sed -r "s/@[^ \\t][^ \\t]*/<USERNAME>/g" ' + path + ' | sed -r "s/(http[s]?:\/[^ \\t]*|www\.[^ \\t]*)/<URL>/g" > ' + path + '.tmp'
    os.system(cmd)
    cmd = 'mv ' + path + '.tmp ' + path
    os.system(cmd)

