
sed -i ':a;N;$!ba;s/\n\n\n/\n\n/g' data/[dt]*

cut -f 1-3 data/dev_O > data/dev_O.norm
cut -f 1-3 data/test_O > data/test_O.norm
cut -f 1-3 data/test_L > data/test_L.norm
cut -f 1 data/train_O | sort | uniq > data/knowns



for file in data/*[LO]
do
    cut -f 4 $file > "$file".pos
    cut -f 1 $file > "$file".raw
    cut -f 1-3 $file > "$file".norm
done
cut -f 1 data/train_O > tmp
cut -f 2 data/train_O > data/train_O.pos
paste data/train_O tmp > data/train_O.norm
rm tmp

cut -f 1 data/dev_O > data/dev_O.raw
cut -f 3 data/dev_O > data/dev_O.gold
cut -f 4 data/dev_O > data/dev_O.pos


